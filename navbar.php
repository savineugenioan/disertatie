    <?php
    include('util/DB.php');
    if (isset($_SESSION['isLogged']))
      $isLogged = $_SESSION['isLogged'];
    else
      $isLogged = 0;
    $suma = 0;
    $is_admin= 0;
    $username=" ";
    if(isset($_SESSION['id_User'])){
      $id_User=$_SESSION['id_User'];
      $query = mysqli_query($dbconnect, "SELECT is_admin,username FROM USERS WHERE id_User=$id_User;")
      or die(mysqli_error($dbconnect));
      $rez = mysqli_fetch_all($query)[0];
      $is_admin = $rez[0];
      $username = $rez[1];
      echo "<script>var is_admin=$is_admin;</script>";
    }

    $query = mysqli_query($dbconnect, "SELECT * FROM PRODUCT")
      or die(mysqli_error($dbconnect));


    while ($row = mysqli_fetch_array($query)) {
      if (isset($_SESSION['cart'][$row[0]]['count']))
        $suma +=$_SESSION['cart'][$row[0]]['count'];
    }
    ?>

    <script>
      function create_dreapta() {
        var elem;
        if (<?php echo $isLogged; ?> == 1) {
              // '<img src="<?php //echo ($user_img!='')? $user_img : 'dragon'; ?>" alt="Avatar" id="poza_profil" >' +
          elem =
            '<li class="nav-item dropdown" id="dropdown" style="margin-top:5px;">' + 
            '<a class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" style="text-decoration: none;margin-top:10px;">' +

            '<div id="poza_profil"><span><?php echo strtoupper($username[0]); ?></span></div>'+
            '</a>' +
            '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"  style="z-index:99;"> ' +
            '<a class="dropdown-item" href="comenzi_anterioare.php">Comenzi Anterioare</a>'+
            '<a class="dropdown-item" href="date_personale.php">Date Personale</a>';
            if(is_admin ==1){
              elem+=
              '<a class="dropdown-item" href="rapoarte/produse_admin.php">Meniu Admin</a>';
            }
            elem+=
            // '<div class="dropdown-divider"></div>' +
            '<a class="dropdown-item" href="index.php" onclick="deconectare()">Deconectare</a>' +
            '</div>' +
            '</li>';

        } else {
          elem =
            '<li class="nav-item" id="btn_conectare">' +
            '<button class="btn btn-primary" style="margin-top:5px;" ><a style="color:white" href="login.php">Login</a></button>' +
            '</li>';
        }
        document.getElementById('navbar_dreapta').innerHTML += elem;
      }

      function deconectare() {
        sessionStorage.setItem('isLogged', 0);
        $.post('util/login_verif.php', {
          deconect: 1
        });
      }

      function verif_login() {
        $.get("util/isLogged.php",{},function(data){
          console.log(data);
          if(data ==1)
            window.location = 'shopping_cart.php';
          else
            window.location = 'login.php';

        })  
      }
    </script>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark navbar1">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-brand" href="index.php">
              <svg width="2em" height="2em" viewBox="0 0 20 20" fill="white" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.5 12.995V16.5a.5.5 0 01-.5.5H4a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5h-4a.5.5 0 01-.5-.5V13c0-.25-.25-.5-.5-.5H9c-.25 0-.5.25-.5.495z">
                </path>
                <path fill-rule="evenodd" d="M15 4.5V8l-2-2V4.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd">
                </path>
              </svg>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="produse.php">Produse</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="locatii.php">Locatii</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="despre_noi.php">Despre Noi</a>
          </li>

        </ul>
        <ul class="navbar-nav" id="navbar_dreapta">
          <li class="nav-item dropdown" >
            <a class="nav-brand" href="shopping_cart.php" onclick="verif_login()">
            <svg width="2.5em" height="2.5em" viewBox="0 0 15 16" fill="white" xmlns="http://www.w3.org/2000/svg" >
              <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0
               1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11
                3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
            </svg>
            </a>
          <li  style="margin-left:5px;margin-right:10px;margin-top:5px;">
            <span id="cart_count" class="badge badge-light"><?php echo $suma; ?></span>
          </li>
          </li>
        </ul>
      </div>
      <p style="position:fixed"></p>
    </nav>
    <script>
      create_dreapta();
    </script>
    <div style="height:60px;background:transparent;"></div>