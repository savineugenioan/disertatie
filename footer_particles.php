<?php
    $path_prefix = '';
    if (strpos($_SERVER["REQUEST_URI"], '/rapoarte/')!== false) { 
      $path_prefix.="../";
  }
?>
<div class="count-particles">
  <span class="js-count-particles">--</span> particles
</div>

<!-- particles.js container -->
<div id="particles-js"  style="position:fixed"></div>

<!-- scripts -->
<script src="<?php echo $path_prefix; ?>js/particles.js"></script>
<script src="<?php echo $path_prefix; ?>js/app.js"></script>

<!-- stats.js -->
<script src="<?php echo $path_prefix; ?>js/lib/stats.js"></script>

<script>
  var count_particles, stats, update;
  stats = new Stats;
  stats.setMode(1);
  stats.domElement.style.position = 'absolute';
  stats.domElement.style.left = '0px';
  stats.domElement.style.top = '5px';
  document.body.appendChild(stats.domElement);
  count_particles = document.querySelector('.js-count-particles');
  update = function() {
    stats.begin();
    stats.end();
    if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
      count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
    }
    requestAnimationFrame(update);
  };
  requestAnimationFrame(update);
</script>
