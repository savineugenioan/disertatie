<?php

include("header.php");

include_once("header_particles.php");

include_once("footer_particles.php"); 

?>

<link rel="stylesheet" type="text/css" href="css/login.css">

</head>

<body>

    <?php
    include("navbar.php");
    include("captcha_header.php");
    ?>



    <form class="form-group " id="form" action="util/c_cont.php" method="post" onsubmit="return validate()">

            <p class="h4 mb-4 text-center">Creare Cont</p>

            <label for="textInput">Username</label>

            <input type="text" id="username" name="username" class="form-control mb-4" placeholder="Username" value = "<?php if(isset($_GET['username'])) echo $_GET['username']; ?>">



            <label for="passwdInput">Parola</label>

            <input type="password" id="parola" class="form-control mb-4" placeholder="Parola">



            <label for="passwdInput">Confirmare Parola</label>

            <input type="password" id="parola_confirm" name="parola" class="form-control mb-4" placeholder="Parola">



            <label for="emailInput">Emaill</label>

            <input type="email" id="email" name="email" class="form-control mb-4" placeholder="Email" value = "<?php if(isset($_GET['email']))  echo $_GET['email']; ?>">



            <label for="textInput">Adresa</label>

            <input type="text" id="adresa" name="adresa" class="form-control mb-4" placeholder="Adresa" value = "<?php if(isset($_GET['adresa']))  echo $_GET['adresa']; ?>">

            <input  type="hidden" name="token" id="token" value="token"  />
            <button class="btn btn-info btn-block my-4 g-recaptcha" >Creare Utilizator</button>

    </form>

    <script>

    function validate(){

        let username = document.getElementById('username').value;

        let parola = document.getElementById('parola').value;

        let parola_confirm = document.getElementById('parola_confirm').value;

        let email = document.getElementById('email').value;

        let adresa = document.getElementById('adresa').value;

        let alert1 = document.getElementById('alert1');

        

        if(username =="" || parola =="" || parola_confirm=="" || email =="" || adresa ==""){

            if(alert1 == null){

                string = '<div class="alert alert-danger" id="alert1" role="alert">Completati toate datele!</div>';

                let div = document.createElement("div");div.innerHTML = string;

                console.log(div);

                let form = document.getElementById('form');

                form.insertBefore(div,form.childNodes[22]);

                return false;

            }

            alert1.innerText = "Completati toate datele!";

            return false;

        }

        if(parola != parola_confirm){

            if(alert1 == null){

                string = '<div class="alert alert-danger" id="alert1" role="alert">Parola nu coincide cu Confirmarea Parolei!</div>';

                let div = document.createElement("div");div.innerHTML = string;

                console.log(div);

                let form = document.getElementById('form');

                form.insertBefore(div,form.childNodes[22]);

                return false;

            }

            alert1.innerText = "Parola nu coincide cu Confirmarea Parolei!";

            return false;



        return false;

        }

        return true;

    }

    </script>

</body>

<script>

    function afis_error(error){

        let alert1 = document.getElementById('alert1');

        if(alert1 == null){

                string = '<div class="alert alert-danger" id="alert1" role="alert">'+error+'</div>';

                let div = document.createElement("div");div.innerHTML = string;

                console.log(div);

                let form = document.getElementById('form');

                form.insertBefore(div,form.childNodes[22]);

            }

    }

</script>

<?php

    if(isset($_GET['error']))

    echo "<script>afis_error('".$_GET['error']."');</script>";

?>