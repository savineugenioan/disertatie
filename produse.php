<!DOCTYPE html>

<html lang="en">



<head>
  <?php
    include("header.php");
  ?>
</head>



<body>

  <?php

  include("navbar.php");

  include_once("header_particles.php");

  include_once("footer_particles.php"); 

  $sql = "SELECT * FROM PRODUCT WHERE display_product = 1 ";
  if(isset($_GET['id_categorie']))
    $sql.=" AND id_categorie = '".$_GET['id_categorie']."' ";

  $query = mysqli_query($dbconnect, $sql);

  while ($row = $query->fetch_assoc()) {

    if (isset($_SESSION['cart'][$row['item_id']])) {

      $v[$row['item_id']]['count'] = $_SESSION['cart'][$row['item_id']]['count'];

    } else {

      $v[$row['item_id']]['count'] = 0;

    }

    $v[$row['item_id']]['item_id'] = $row['item_id'];

    $v[$row['item_id']]['item_name'] = $row['item_name'];

    $v[$row['item_id']]['item_price'] = $row['item_price'];

    $v[$row['item_id']]['item_image'] = $row['item_image'];

    //echo var_dump($v);

  }

  ?>

  <div style="margin-top:20px;margin-left:30px;">
  <?php
    $sql1 = "SELECT * FROM CATEGORIES ";
    $query1 = mysqli_query($dbconnect, $sql1);

    echo '<button style="position:relative;display:inline;" onclick="location.href=\'produse.php\'" class="btn-gradient">All</button>';
    while ($row1 = $query1->fetch_assoc()) {
      echo '<button style="position:relative;" onclick="location.href=\'produse.php?id_categorie='.$row1['id_categorie'].'\'" class="btn-gradient">'.$row1['den_categorie'].'</button>';
    }
    ?>
  </div>

  <?php

    foreach ($v as $key=>$value) {

      //echo var_dump($value)." ";

      $image= $v[$key]['item_image'];

      echo "

    <div class='card' >

      <img src='$image' class='card-img-top' onerror='this.src =".'"'.$def_source.'"'."' alt='...'>

      <div class='card-body'>

        <h5 class='card-title'>" . $v[$key]['item_name'] . "</h5>

        <p class='card-text'>" . $v[$key]['item_price'] . "</p>

      </div>

      <div class='card-footer'>

        <button class='card-footer-item btn btn-primary' style='float:left;' onclick='scot_produs(this,".$v[$key]['item_id'].");'>-</button>

        <span class='card-footer-item' style='top: 35%;' id='produs_count'>";

        if ($v[$key]['count'] == 0)

        echo "Add Item To Cart";

        else

        echo $v[$key]['count'];

        echo "

        </span>

        <button class='card-footer-item btn btn-primary' style='float:right;' onclick='adaug_produs(this,".$v[$key]['item_id'].");'>+</button>

        </div>

    </div>";

    }

  ?>

  </div>

  </body>

  <script>

    var cart_count = document.getElementById('cart_count');

    function adaug_produs(el, id) {

      let x = parseInt(cart_count.innerText);

      let y = el.parentNode.childNodes[3];

      //console.log(parseInt(y.innerText));

      if(isNaN(parseInt(y.innerText))){

                y.innerText = 1;

      }

      else

         y.innerText = parseInt(y.innerText) +1;

      let suma = x;

      cart_count.innerText = suma + 1;

      post_prod('+',el.parentNode.parentNode.childNodes[3].childNodes[1].innerText,id);



    }

    function post_prod(op,el,id){

      console.log(op+' '+el+' '+' '+id);

      $.post('util/cart_modif.php', {

        op: op,

        produs: el,

        id:id

      });

    }



    function scot_produs(el,id) {

      let x = parseInt(cart_count.innerText); // cart

      let y = el.parentNode.childNodes[3];  // produs

      console.log(id);

      //console.log(parseInt(y.innerText));

      if (!isNaN(parseInt(y.innerText))) {

        let suma = x;

        cart_count.innerText = suma - 1;

        if(parseInt(y.innerText) > 1)

          y.innerText = parseInt(y.innerText) -1;

        else{

          y.innerText="Add Item To Cart";

      }

        post_prod('-',el.parentNode.parentNode.childNodes[3].childNodes[1].innerText,id);

      }

      else{

        y.innerText="Add Item To Cart";

      }



    }

  </script>



</html>