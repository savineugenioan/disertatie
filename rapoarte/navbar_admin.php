<!DOCTYPE html>
<html lang="en">
<?php
include_once("../util/DB.php");
include_once("header_admin.php");
define ('ROOT',  "../");
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    
?>
<script>
        function logout(){
            $.ajax({
                url : '../util/logout.php',
                type : 'POST',
                success : function(data) {              
                    window.location.replace("index.php");
                }
                });
        }
    </script>
<body>
<?php
    if (isset($_SESSION['isLogged']))
      $isLogged = $_SESSION['isLogged'];
    else
      $isLogged = 0;
      ?>
    <nav class="navbar navbar-fixed-top navbar-expand-md navbar-dark bg-dark navbar1" style="z-index:20;">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse center"  id="navbarColor02">
        <ul class="navbar-nav item-center mr-auto">
          <li class="nav-item active" style="margin-top:-3px;">
            <a class="nav-link" href="../index.php">
              <svg class="bi bi-house-fill" width="1.5em" height="1.5em" viewBox="0 0 20 20" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M8.5 12.995V16.5a.5.5 0 01-.5.5H4a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5h-4a.5.5 0 01-.5-.5V13c0-.25-.25-.5-.5-.5H9c-.25 0-.5.25-.5.495z">
                </path>
                <path fill-rule="evenodd" d="M15 4.5V8l-2-2V4.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd">
                </path>
              </svg>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="produse_admin.php">Produse</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="clienti_admin.php">Clienti</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="comenzi_principal_admin.php">Comenzi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="rapoarte.php">Rapoarte</a>
          </li>

        </ul>
      </div>
    </nav>