<!DOCTYPE html>
<html lang="en">
<?php
$def_source = "dragon.jpg";
?>
</head>
<body>
    <?php
    include('navbar_admin.php');
    include_once("../header_particles.php");
    include_once("../footer_particles.php"); 
    $id_produs = $_GET['id'];
    //echo $_GET['id'];
    $sql = "SELECT * FROM PRODUCT WHERE item_id=$id_produs";
    //echo $sql;
    $query = mysqli_query($dbconnect, $sql) or die(mysqli_error($dbconnect));
    $v = mysqli_fetch_all($query)[0];
    //echo var_dump($v);
    $denumire = $v[2];
    $brand = $v[1];
    $pret = $v[3];
    $displayed = $v[6];
    $category = $v[7];
    ?>
    </br>
    <form class="form-group"  enctype="multipart/form-data" method="post" id="form" action="../util/edit_produs.php?id_Produs=<?php echo $id_produs; ?>&old_name=<?php echo $denumire; ?>" onsubmit="return validate()">
        <div class="form-group" style="vertical-align:middle;">
            <img width="200px" style="margin: auto auto;display:block;"  height="200px" id="image" src="<?php echo "../".$v[4]; ?>" onerror='this.src ="<?php echo $def_source; ?>"' alt='...'></img>
            <label >Poza</label>
            <input type="file" class="form-control mb-4 form-img" name="image" id="image" accept="image/png, image/jpeg" onchange="handleFiles(this.files)">
            <label >Denumire Produs</label>
            <input width="100%" class="form-control mb-4"  type="text" id="denumire" class="fadeIn second" name="denumire" placeholder="Denumire" value="<?php echo $denumire; ?>">
            <label >Brand Produs</label>
            <input width="100%" class="form-control mb-4"  type="text" id="brand" class="fadeIn second" name="brand" placeholder="Brand" value="<?php echo $brand ; ?>">

            <label >Pret Unitar</label>
            <input width="100%" class="form-control mb-4" type="number" id="pret" class="fadeIn third" min="0" name="pret" placeholder="Pret" value="<?php echo $pret ;?>">
            <label >Stare Afisare Publica</label>
            <select name="display_product" id="display_product" class="form-control mb-4">
                <option value="0" <?php if($displayed==0) echo ' selected '; ?> >NEAFISAT</option>
                <option value="1" <?php if($displayed==1) echo ' selected '; ?> >AFISAT</option>
            </select>
            <label >Categorie</label>
            <select width="100%" class="form-control mb-4"  name="id_categorie" id="id_categorie">
                <?php 
                 $sql1 = "SELECT * FROM CATEGORIES ";
                 $query1 = mysqli_query($dbconnect, $sql1);            
                 while ($row1 = $query1->fetch_assoc()) {
                   echo '<option '.(($row1['id_categorie']==$category)? 'selected' : '').' value="'.$row1['id_categorie'].'">'.$row1['den_categorie'].'</option>';
                 }
                ?>
            </select>
        </div>
        <button type="submit" id="submit" class="btn btn-info btn-block my-4" >Salveaza Datele</button>
    </form>

</body>
<script>
    var checked_photo = 0;
    document.getElementById('form').action += '&checked_photo=0';

    function handleFiles(files) {

        if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = function() {
                document.getElementById('image').src = fr.result;
                checked_photo = 1;
                var x = document.getElementById('form').action;
                x = x.substr(0, x.length - 1);
                document.getElementById('form').action = x + checked_photo;
                //console.log(x);
            }
            fr.readAsDataURL(files[0]);
        }
    }

    function change_photo() {
        return checked_photo;
    }
</script>
<script>
    function validate() {
        let denumire = document.getElementById("denumire").value;
        let pret = document.getElementById("pret").value;
        let alert = document.getElementById("alert");
        if ( denumire == "" || pret == "") {
            if (alert != null)
                return false;
            string = '<div class="alert alert-danger" id="alert" role="alert"> Completati toate campurile!</div>';
            var div = document.createElement('div');
            div.innerHTML = string;
            var list = document.getElementById("form");
            list.insertBefore(div, list.childNodes[3]);
            return false;
        }
        return true;
    }
</script>

</html>