<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include("navbar_admin.php");
    include_once("../header_particles.php");
    include_once("../footer_particles.php"); 
    $def_source = "dragon.jpg";
    ?>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
</head>
<style>
    body {
        background-color: #454d55;
    }

    .iframe_pdf {
        position: relative !important;
        top: 0 !important;
        left: 0 !important;
        width: 100% !important;
        height: 100vh !important;
    }

    .ui-dialog {
        width: 90% !important;
        height: 100vh !important;

    }
</style>

<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark navbar1" style="margin-top:60px;">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse center"  id="navbarColor03">
        <ul class="navbar-nav item-center mr-auto">
          <li class="nav-item">
            <a class="nav-link" onclick="redirectPhp(this.id)" id="raport_vanzari" href="#">Raport Vanzari</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="redirectPhp(this.id)" id="raport_cmv_produs" href="#">Cel mai vandut produs</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="redirectPhp(this.id)" id="raport_comenzi_user" href="#">Comenzi in functie de user</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="redirectPhp(this.id)" id="raport_timp" href="#">Comenzi intr-un interval de timp</a>
          </li>

        </ul>
      </div>
    </nav>
<iframe id="iframe_pdf" class="iframe_pdf" src="raport_vanzari.php" style="margin-top:110px;"></iframe>
</body>
<script>
    $(document).ready(function() {
        $('.dataTables_length').addClass('bs-select');
    });

    function redirectPhp(id) {
        document.getElementById("iframe_pdf").src = id+".php";
    }
</script>