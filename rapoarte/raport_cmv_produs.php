<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include("header_raport.php");
    include_once("../header_particles.php");
    include_once("../footer_particles.php"); 
    $def_source = "dragon.jpg";
    ?>

</head>
<body>
  <?php
  $id_user = $_SESSION['id_User'];
  ?>
  <table style="position:relative;width:100%" id="dtBasicExample" class="table table-dark table-striped table-bordered " cellspacing="0" width="100%">
    <thead>
      <tr>
        <th class="th-sm" scope="col">Poza</th>
        <th class="th-sm" scope="col">Nume Produs</th>
        <th class="th-sm" scope="col">Brand</th>
        <th class="th-sm" scope="col">Pret Unitar</th>
        <th class="th-sm" scope="col">Cantitate Totala Vanduta</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $query = mysqli_query($dbconnect, "SELECT * FROM (SELECT p.*,cd.Produs, SUM(Cantitate) As Total_cantitate FROM COMENZI_DETALII cd INNER JOIN PRODUCT p ON cd.item_id = p.item_id GROUP By cd.item_id) t
      WHERE t.Total_cantitate = ( SELECT MAX(Total_cantitate) FROM (SELECT SUM(Cantitate) As Total_cantitate FROM COMENZI_DETALII cd INNER JOIN PRODUCT pr ON cd.item_id = pr.item_id GROUP By cd.item_id) x ) LIMIT 1");

      while ($row = mysqli_fetch_assoc($query)) {
        $id = $row['item_id'];
        $image = $row['item_image'];
        $name = $row['item_name'];
        $price = $row['item_price'];
        $brand = $row['item_brand'];
        echo "
    <tr>
      <td><img class='t_img' width='50' height='60' src='../$image' onerror='this.src =" . '"' . $def_source . '"' . "'</td>
      <td scope='row'>$name</td>
      <td scope='row'>$brand</td>
      <td>$price</td>
      <td>".$row['Total_cantitate']."</td>
    </tr>";
      }
      ?>
    </tbody>
  </table>
  <script>
    $(document).ready(function() {
      $('.dataTables_length').addClass('bs-select');
    });

    function alerta() {
      var r = confirm("Doriti sa stergeti acest produs?");
      if (r == true) {
        return true
      } else {
        return false;
      }
    }
    function redirect(){
      window.location.assign('adaugare_produs.php');
    }
  </script>
</body>

</html>