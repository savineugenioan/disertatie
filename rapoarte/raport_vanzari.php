<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include("header_raport.php");
    include_once("../header_particles.php");
    include_once("../footer_particles.php"); 
    $def_source = "dragon.jpg";
    ?>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

</head>
<body>
  <?php
  $id_user = $_SESSION['id_User'];
  ?>
  <table style="position:relative;width:100%" id="dtBasicExample" class="table table-dark table-striped table-bordered " cellspacing="0" width="100%">
    <thead>
      <tr>
        <th class="th-sm" scope="col">Poza</th>
        <th class="th-sm" scope="col">Nume Produs</th>
        <th class="th-sm" scope="col">Brand</th>
        <th class="th-sm" scope="col">Pret Unitar</th>
        <th class="th-sm" scope="col">Cantitate Totala Vanduta</th>
        <th class="th-sm" scope="col">Total incasat</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $query = mysqli_query($dbconnect, "SELECT  pr.*, SUM(`Cantitate`) AS Total FROM COMENZI_DETALII cd,PRODUCT pr
       WHERE pr.item_id = cd.item_id GROUP BY Produs ORDER BY Total DESC");

      while ($row = mysqli_fetch_assoc($query)) {
        $id = $row['item_id'];
        $image = "../".$row['item_image'];
        $name = $row['item_name'];
        $price = $row['item_price'];
        $brand = $row['item_brand'];
        echo "
    <tr>
      <td><img class='t_img' width='50' height='60' src='$image' onerror='this.src =" . '"' . $def_source . '"' . "'</td>
      <td scope='row'>$name</td>
      <td scope='row'>$brand</td>
      <td>$price</td>
      <td>".$row['Total']."</td>
      <td>".($row['Total']*$row['item_price'])."</td>
    </tr>";
      }
      ?>
    </tbody>
  </table>
  <script>
    $(document).ready(function() {
      $('.dataTables_length').addClass('bs-select');
    });

    function alerta() {
      var r = confirm("Doriti sa stergeti acest produs?");
      if (r == true) {
        return true
      } else {
        return false;
      }
    }
    function redirect(){
      window.location.assign('adaugare_produs.php');
    }
  </script>
</body>

</html>