<!DOCTYPE html>
<html lang="en">
<?php
$def_source = "dragon.jpg";
?>
</head>
<body>
    <?php
    include('navbar_admin.php');
    include_once("../header_particles.php");
    include_once("../footer_particles.php"); 
    $id_comanda = $_GET['id'];
    //echo $_GET['id'];
    $sql = "SELECT * FROM COMENZI WHERE id_Comanda=$id_comanda";
    //echo $sql;
    $query = mysqli_query($dbconnect, $sql) or die(mysqli_error($dbconnect));
    $v = mysqli_fetch_all($query)[0];
    //echo var_dump($v);
    ?>
    </br>
    <form class="form-group"  enctype="multipart/form-data" method="post" id="form" action="../util/edit_comanda.php?id=<?php echo $id_comanda; ?>" onsubmit="return validate()">
    <div class="form-group" style="vertical-align:middle;">
        
        <label >Nr. Comanda</label>
        <input width="100%" class="form-control mb-4" readonly type="text" id="id" class="fadeIn second" name="denumire" placeholder="Denumire" value=<?php echo $v[0] ?>>
        <label >Data emitere</label>
        <input width="100%" class="form-control mb-4" readonly type="text" id="data" class="fadeIn second" name="denumire" placeholder="Denumire" value=<?php echo $v[1] ?>>
        <label >Status</label>
        <select name="achitat" id="achitat" class="form-control ">
            <option value="0">NEACHITAT</option>
            <option value="1">ACHITAT</option>
            </select>        </div>
        <button type="submit" id="submit" class="btn btn-info btn-block my-4" >Salveaza Datele</button>
    </form>

</body>
<script>
    var status = '<?php echo $v[3] ?>';
    if(status == "NEACHITAT"){
            document.getElementById("achitat").selectedIndex =0;
    }
    else{
        document.getElementById("achitat").selectedIndex =1;
    }


    function validate(){
        let id = document.getElementById('id').value;
        let data = document.getElementById('data').value;
        let achitat = document.getElementById('achitat').value;
        let alert1 = document.getElementById('alert1');
        
        if(id =="" || data =="" || achitat =="" ){
            if(alert1 == null){
                string = '<div class="alert alert-danger" id="alert1" role="alert">Completati toate datele!</div>';
                let div = document.createElement("div");div.innerHTML = string;
                console.log(div);
                let form = document.getElementById('form');
                form.insertBefore(div,form.childNodes[22]);
                return false;
            }
            alert1.innerText = "Completati toate datele!";
            return false;
        }
        return true;
    }
</script>
</html>