<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    include("header_raport.php");
    include_once("../header_particles.php");
    include_once("../footer_particles.php"); 
    $def_source = "dragon.jpg";
    ?>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/comenzi.css">
    <link rel="stylesheet" type="text/css" href="../css/login.css">
</head>
<style>
    body{
        background-color: #454d55;
    }

    .iframe_pdf {
        position: relative !important;
        top: 0  !important; left: 0  !important;
        width:100%  !important;
        height: 100vh !important;
    }
    .ui-dialog{
        width:90%  !important;
        height: 100vh !important;
        
    }
    input[type="date"]{
        border: 1px solid black;
        border-radius: 5px;
        margin: 15px 15px 15px 15px;

    }
</style>
<?php
$id_user=$_SESSION['id_User'];
$sql ="SELECT c.Id_comanda,c.c_date,(SELECT username FROM USERS WHERE id_User = c.Id_User) AS USER ,
c.status_comanda,(SELECT SUM(`Cantitate`*`Pret_Unitar`) from COMENZI_DETALII WHERE id_comanda=c.Id_Comanda GROUP BY Id_comanda ) AS Total
 FROM COMENZI c INNER JOIN USERS u ON c.id_User = u.id_User";
if(isset($_GET['start']) && isset($_GET['end']) && $_GET['start'] != "" && $_GET['end']!=""){
    $sql.=" WHERE c.c_date >= '".$_GET['start']." 00:00:00' AND c.c_date <='".$_GET['end']." 23:59:59';";
}

?>

<?php
  $id_user=$_SESSION['id_User'];
  ?>
  
  <div>
  <input type="date" id="start" name="trip-start" style="position:absolute;margin-left:15px;margin-top:50px;">
  <input type="date" id="end"   name="trip-start" style="position:absolute;;margin-left:185px;margin-top:50px;">
    <button class="btn btn-primary" style ="display:inline;position:absolute;;margin-left:375px;margin-top:45px;" onclick="redirect()">Cautare</button>
  </div>
<table  style="position:absolute;margin-top:95px;width:100%"id="dtBasicExample" class="table table-dark table-striped table-bordered " cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm"scope="col">Username</th>
      <th class="th-sm"scope="col">Nr. Comanda</th>
      <th class="th-sm"scope="col">Data</th>
      <th class="th-sm"scope="col">Pret Total</th>
      <th class="th-sm"scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
  <?php
    $query = mysqli_query($dbconnect, $sql) or die(mysqli_error($dbconnect));

    while($row = mysqli_fetch_array($query)){
    echo "
    <tr>
      <th scope='row'>$row[2]</th>
      <th scope='row'>$row[0]</th>
      <td>$row[1]</td>
      <td>$row[4]</td>
      <td>$row[3]</td>
    </tr>";
    }
    ?>
  </tbody>
</table>
<div id="dialog" style="display:none">
    <iframe id="iframe_pdf" class="iframe_pdf" src=""></iframe>
</div>
<script>
        $(document).ready(function () {
        $('.dataTables_length').addClass('bs-select');
        });
    function openFactura(id){
			$("#iframe_pdf").attr("src","comenzi_admin.php?id_comanda="+id);
			$( "#dialog" ).dialog({
				autoOpen: false, 
				modal: true,
				title: "FACTURA",
				hide: "puff",
				show : "slide",
				position: {
					my: " top ",
					at: " top ",
					collision: "fit",
				 }
			 });
			$("#dialog").dialog("open");
    }
</script>
  <script>
    $(document).ready(function() {
      $('.dataTables_length').addClass('bs-select');
      var newDate = new Date();
      newDate = newDate.toISOString().split("T")[0];
      document.getElementById('end').min = newDate;
      var newDate = new Date();
      newDate.setDate(newDate.getDate() +1);
      newDate = newDate.toISOString().split("T")[0];
      document.getElementById('end').value = newDate;
      document.getElementById('start').max = newDate;
      var newDate = new Date();
      newDate.setMonth(newDate.getMonth() - 2);
      var newDate = newDate.toISOString().split("T")[0];
      document.getElementById('start').value = newDate;
    });

    function alerta() {
      var r = confirm("Doriti sa stergeti acest produs?");
      if (r == true) {
        return true
      } else {
        return false;
      }
    }
    function redirect(){
        var start = document.getElementById('start').value;
        var end = document.getElementById('end').value;
        window.location.assign('raport_timp.php?start='+start+"&end="+end);

    }
</script>
</body>

</html>