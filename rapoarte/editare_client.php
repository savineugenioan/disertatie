<!DOCTYPE html>
<html lang="en">
<head>
  <?php
  include("navbar_admin.php");
  include_once("../header_particles.php");
  include_once("../footer_particles.php"); 
  $def_source = "dragon.jpg";
  ?>
</head>

<body>
    <?php
    $id_User=$_GET['id_User'];
    $sql = "SELECT username,is_admin,email,adress FROM USERS WHERE Id_User=$id_User;";
    $query = mysqli_query($dbconnect, $sql);
    $row = mysqli_fetch_row($query);
    //echo var_dump($row);
    ?>
    <div style="height:90px;"></div>
    <form class="form-group " id="form" action="../util/edit_client.php?id_User=<?php echo $id_User; ?>" method="post" onsubmit="return validate()">
            <p class="h4 mb-4 text-center">Editare Cont</p>
        <div class="form-group">
            <label for="textInput">Username</label>
            <input type="text" id="username" readonly name="username" class="form-control mb-4" placeholder="Username" value="<?php echo $row[0] ?>">

            <label for="is_admin">Admin</label>
            <select name="is_admin" id="is_admin" class="form-control mb-4">
            <option value="0">0</option>
            <option value="1">1</option>
            </select>
            <label for="emailInput">Emaill</label>
            <input type="email" readonly id="email" name="email" class="form-control mb-4" placeholder="Email" value="<?php echo $row[2] ?>">

            <label for="textInput">Adresa</label>
            <input type="text" id="adresa" name="adresa" class="form-control mb-4" placeholder="Adresa" value="<?php echo $row[3] ?>">
        </div>
            <button class="btn btn-info btn-block my-4" >Editare Utilizator</button>
    </form>
    <script>
    document.getElementById("is_admin").selectedIndex = <?php echo $row[1] ?>;

    function validate(){
        let username = document.getElementById('username').value;
        let email = document.getElementById('email').value;
        let adresa = document.getElementById('adresa').value;
        let alert1 = document.getElementById('alert1');
        
        if(username =="" || email =="" || username =="" || adresa ==""){
            if(alert1 == null){
                string = '<div class="alert alert-danger" id="alert1" role="alert">Completati toate datele!</div>';
                let div = document.createElement("div");div.innerHTML = string;
                console.log(div);
                let form = document.getElementById('form');
                form.insertBefore(div,form.childNodes[22]);
                return false;
            }
            alert1.innerText = "Completati toate datele!";
            return false;
        }
        return true;
    }
    </script>
</body>