<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include("navbar_admin.php");
  include_once("../header_dataTable.php");
  include_once("../header_particles.php");
  include_once("../footer_particles.php"); 
  $def_source = "dragon.jpg";
  ?>
</head>
<style>
    body{
        background-color: #454d55;
    }
</style>
<?php
$id_user = $_SESSION['id_User'];
$sql = "SELECT * FROM USERS";

?>

<?php
$id_user = $_SESSION['id_User'];
?>
<table style="position:absolute;width:100;margin-top:60px;" id="dtBasicExample" class="table table-dark table-striped table-bordered " cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm" scope="col">User Id</th>
      <th class="th-sm" scope="col">Tip cont</th>
      <th class="th-sm" scope="col">Username</th>
      <th class="th-sm" scope="col">Email</th>
      <th class="th-sm" scope="col">Adresa</th>
      <th class="th-sm" scope="col">Edit</th>
      <th class="th-sm" scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $query = mysqli_query($dbconnect, $sql) or die(mysqli_error($dbconnect));

    while ($row = mysqli_fetch_array($query)) {
      echo "
    <tr>
      <th scope='row'>$row[0]</th>
      <th scope='row'>$row[1]</th>
      <td>$row[2]</td>
      <td>$row[4]</td>
      <td>$row[5]</td>
      <td><a href='editare_client.php?id_User=$row[0]'>Edit</a></td>
      <td><a href='../util/delete_client.php?id_User=$row[0]' onclick='return alerta()' >Delete</a></td>
    </tr>";
    }
    ?>
  </tbody>
</table>
<script>
  $(document).ready(function() {
    $('#dtBasicExample').DataTable({
      "pagingType": "simple"
    });
    $('.dataTables_length').addClass('bs-select');
  });

  function alerta() {
    var r = confirm("Doriti sa stergeti acest client?");
    if (r == true) {
      return true
    } else {
      return false;
    }
  }
</script>