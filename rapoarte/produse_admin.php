<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include("navbar_admin.php");
  include_once("../header_dataTable.php");
  include_once("../header_particles.php");
  include_once("../footer_particles.php"); 
  $def_source = "dragon.jpg";
  ?>
</head>
<style>
    body{
        background-color: #454d55;
    }
</style>

<body>
  <?php
  $id_user = $_SESSION['id_User'];
  ?>
  <button class="btn-gradient" style ="margin-bottom:5px;display:inline;position:relative;margin-top:80px;margin-left:35px;" onclick="redirect()">Adaugare Produs</button>
  <table style="position:relative;margin-top:10px;width:100%" id="dtBasicExample" class="table table-dark table-striped table-bordered " cellspacing="0" width="100%">
    <thead>
      <tr>
        <th class="th-sm" scope="col">Poza</th>
        <th class="th-sm" scope="col">Nume Produs</th>
        <th class="th-sm" scope="col">Brand</th>
        <th class="th-sm" scope="col">Pret Unitar</th>
        <th class="th-sm" scope="col">Starea afisarii</th>
        <th class="th-sm" scope="col">Edit</th>
        <th class="th-sm" scope="col">Delete</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $query = mysqli_query($dbconnect, "SELECT * FROM PRODUCT") or die(mysqli_error($dbconnect));
      while ($row = mysqli_fetch_assoc($query)) {
        $id = $row['item_id'];
        $image = ROOT.$row['item_image'];
        //var_dump($image);
        $name = $row['item_name'];
        $price = $row['item_price'];
        $brand = $row['item_brand'];
        $display = $row['display_product'];
        echo "
    <tr>
      <td><img class='t_img' width='50' height='60' src='$image' onerror='this.src =" . '"' . $def_source . '"' . "'</td>
      <td scope='row'>$name</td>
      <td scope='row'>$brand</td>
      <td scope='row'>$price</td>
      <td scope='row'>".(($display==1)? 'AFISAT' : 'NEAFISAT')."</td>
      <td><a href='editare_produs.php?id=$id'>Edit</a></td>
      <td><a href='../util/delete_produs.php?id=$id&img=$image' onclick='return alerta()'>Delete</a></td>
    </tr>";
      }
      ?>
    </tbody>
  </table>
  <script>
    $(document).ready(function() {
      $('.dataTables_length').addClass('bs-select');
    });

    function alerta() {
      var r = confirm("Doriti sa stergeti acest produs?\nATENTIE! Daca stergeti acest produs, veti produce erori in baza de date pentru comenzile in care acesta apare!\nRecomandam dezafisarea sa!");
      if (r == true) {
        return true
      } else {
        return false;
      }
    }
    function redirect(){
      window.location.assign('adaugare_produs.php');
    }
  </script>
</body>

</html>