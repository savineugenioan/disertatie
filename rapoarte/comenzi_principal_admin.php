<!DOCTYPE html>
<html lang="en">
<head>
  <?php
  include("navbar_admin.php");
  include_once("../header_dataTable.php");
  include_once("../header_particles.php");
  include_once("../footer_particles.php"); 
  $def_source = "dragon.jpg";
  ?>
</head>
<style>
    body{
        background-color: #454d55;
    }

.iframe_pdf {
    position: relative !important;
    top: 0  !important; left: 0  !important;
    width:100%  !important;
    height: 100vh !important;
}
.ui-dialog{
    width:90%  !important;
    height: 100vh !important;
    
}
</style>
<?php
$id_user=$_SESSION['id_User'];
$sql ="SELECT c.Id_comanda,c.c_date,(SELECT username FROM USERS WHERE id_User = c.Id_User) AS USER ,
c.status_comanda,(SELECT SUM(`Cantitate`*`Pret_Unitar`) from COMENZI_DETALII WHERE id_comanda=c.Id_Comanda GROUP BY Id_comanda ) AS Total
 FROM COMENZI c INNER JOIN USERS u ON c.id_User = u.id_User ORDER BY c.Id_comanda DESC";


?>

<?php
  $id_user=$_SESSION['id_User'];
  ?>
<table  style="position:absolute;width:100;margin-top:60px;"id="dtBasicExample" class="table table-dark table-striped table-bordered " cellspacing="0" width="100%">
  <thead>
    <tr>
      <th class="th-sm"scope="col">Username</th>
      <th class="th-sm"scope="col">Nr. Comanda</th>
      <th class="th-sm"scope="col">Data</th>
      <th class="th-sm"scope="col">Pret Total</th>
      <th class="th-sm"scope="col">Status</th>
      <th class="th-sm"scope="col">Edit</th>
      <th class="th-sm"scope="col">Factura</th>
    </tr>
  </thead>
  <tbody>
  <?php
    $query = mysqli_query($dbconnect, $sql) or die(mysqli_error($dbconnect));

    while($row = mysqli_fetch_array($query)){
    echo "
    <tr>
      <th scope='row'>$row[2]</th>
      <th scope='row'>$row[0]</th>
      <td>$row[1]</td>
      <td>$row[4]</td>
      <td>$row[3]</td>
      <td><a href='editare_comanda.php?id=$row[0]' >Edit</a></td>
      <td><a onclick ='openFactura($row[0]);' href='#' >Deschide</a></td>
    </tr>";
    }
    ?>
  </tbody>
</table>
<div id="dialog" style="display:none">
    <iframe id="iframe_pdf" class="iframe_pdf" src=""></iframe>
</div>
<script>
        $(document).ready(function () {
        $('.dataTables_length').addClass('bs-select');
        });
    function openFactura(id){
			$("#iframe_pdf").attr("src","comenzi_admin.php?id_comanda="+id);
			$( "#dialog" ).dialog({
				autoOpen: false, 
				modal: true,
				title: "FACTURA",
				hide: "puff",
				show : "slide",
				position: {
					my: " top ",
					at: " top ",
					collision: "fit",
				 }
			 });
			$("#dialog").dialog("open");
    }
</script>