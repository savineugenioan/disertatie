<?php
include("header.php");
include_once("header_particles.php");
include_once("footer_particles.php"); 
?>
<link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
    <?php
    include("navbar.php");
    ?>

    <form class="form-group " id="form" action="util/login_verif.php" method="post" onsubmit="return validate()">
    <p class="h4 mb-4 text-center">Login</p>
        <div class="form-group">
            <!-- //<p class="h4 mb-4 text-center">Username</p> -->
            <label for="username">Username</label>
            <input type="text" id="username" name="username" class="form-control mb-4" placeholder="Username">

            <label for="passwdInput">Parola</label>
            <input type="password" id="parola" name="parola" class="form-control mb-4" placeholder="Parola">
            <a style="width:100%;margin:auto" href="creare_cont.php">Nu ai cont? Creaza acum unul!</a>
        </div>
        <button class="btn btn-info btn-block my-4" type="submit">Login</button>
        </div>
    </form>
    <script>
    function validate()
        {
        let username = document.getElementById('username').value;
        let parola = document.getElementById('parola').value;
        let alert1 = document.getElementById('alert1');
        
        if(username =="" || parola =="" ){

            if(alert1 == null){
                string = '<div class="alert alert-danger" id="alert1" role="alert">Completati toate datele!</div>';
                let div = document.createElement("div");div.innerHTML = string;
                console.log(div);
                let form = document.getElementById('form');
                form.insertBefore(div,form.childNodes[2]);
                return false;
            }
            alert1.innerText = "Completati toate datele!";
            return false;
        }
        return true;
        }
    </script>
</body>