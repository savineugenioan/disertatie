<?php
include_once("header.php");
include_once("header_particles.php");
include_once("footer_particles.php"); 
?>
<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<script>

</script>

<body>
    <?php
    include("navbar.php");
    $id_User=$_SESSION['id_User'];
    $sql = "SELECT username,password,email,adress FROM USERS WHERE Id_User=$id_User;";
    $password = $row[1];
    $query = mysqli_query($dbconnect, $sql);
    $row = mysqli_fetch_row($query);
    //echo var_dump($row);
    ?>

    <form class="form-group " id="form" action="util/edit_user_curent.php" method="post" onsubmit="return validate()">
            <p class="h4 mb-4 text-center">Editare Cont</p>
            <label for="textInput">Username</label>
            <input type="text" id="username" readonly name="username" class="form-control mb-4" placeholder="Username" value="<?php echo $row[0] ?>">

            <label for="emailInput">Emaill</label>
            <input type="email" readonly id="email" name="email" class="form-control mb-4" placeholder="Email" value="<?php echo $row[2] ?>">

            <label for="textInput">Adresa</label>
            <input type="text" id="adresa" name="adresa" class="form-control mb-4" placeholder="Adresa" value="<?php echo $row[3] ?>">

            <button class="btn btn-info btn-block my-4" >Editare Utilizator</button>
    </form>
    <script>
    function validate(){
        let username = document.getElementById('username').value;
        let email = document.getElementById('email').value;
        let adresa = document.getElementById('adresa').value;
        let alert1 = document.getElementById('alert1');
        
        if(username =="" || email =="" || adresa ==""){
            if(alert1 == null){
                string = '<div class="alert alert-danger" id="alert1" role="alert">Completati toate datele!</div>';
                let div = document.createElement("div");div.innerHTML = string;
                let form = document.getElementById('form');
                form.insertBefore(div,form.childNodes[22]);
                return false;
            }
            alert1.innerText = "Completati toate datele!";
            return false;
        }
        return true;
    }
    </script>
</body>