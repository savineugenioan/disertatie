<?php
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }    
    unset($_SESSION["isLogged"]);
    unset($_SESSION["is_admin"]);
    unset($_SESSION["id_User"]);
?>