<?php
    $path_prefix = '';
    if (strpos($_SERVER["REQUEST_URI"], '/rapoarte/')!== false) { 
      $path_prefix.="../";
  }
?>
<head>
    <meta name="description" content="particles.js is a lightweight JavaScript library for creating particles.">
    <meta name="author" content="Vincent Garreau" />
    <link rel="stylesheet" media="screen" href="<?php echo $path_prefix; ?>css/particles.css">
</head>