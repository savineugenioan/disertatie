<!DOCTYPE html>
<html lang="en">
<head>
<?php
include("header.php");
include_once("header_particles.php");
include_once("footer_particles.php"); 
?>
</head>
<body>

<?php include("navbar.php");?>

<style>
.hr_line{
    margin-left: -19px;
    margin-right: -19px;
    margin-bottom:10px;
    margin-top:0px;
    border: 1px solid white;
    height:0px;"
}
/* Style the tab */
.tab {
  overflow: hidden;
  margin-left: 12px;
}
/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border-top: none;
}
.tab button {
    float: left;
    width: 150px;
    font-size: 24px;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
    height: 45px;
    text-align: center;
    border: none;
    transition: all .4s ease-in-out;
    outline: none;
    background-color: rgb(53, 58, 53);
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: rgb(53, 58, 53);
}

/* Create an active/current tablink class */
.tab button.active {
  background: linear-gradient(to bottom right, red, yellow);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
}
@media (max-width:1500px){
    .container1{
        position:absolute;
        min-width:80%;
        max-width:80%;
    }
}
@media (min-width:1501px) { 
    .container1{
        position:absolute;
        width:1300px;
    }
}
@media (max-width:780px) { 
    iframe{
        margin-bottom:10px;
    }
}
</style>

<div class="container1" style="">
    <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'Bucureşti')" id="defaultOpen">Bucureşti</button>
        <button class="tablinks" onclick="openCity(event, 'Oradea')">Oradea</button>
        <button class="tablinks" onclick="openCity(event, 'Budapesta')">Budapesta</button>
    </div>
    <hr class="hr_line" >
    <div id="Bucureşti" class="tabcontent">
    <div class="row">
        <div class="col-md-8 col-sm-12">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2846.964186619804!2d26.1035055328928!3d44.47490684009013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b1f88055a3b2b3%3A0x6260c30655c540e5!2sBit%20Soft%20SA%20-%20Industria%20Ospitalitatii!5e0!3m2!1sro!2sro!4v1657697614754!5m2!1sro!2sro" style="border:0;width: 100%;position: relative;height:650px;" allowfullscreen="" loading="lazy"></iframe>
              </br>
        </div>
        <div class="col-md-4 col-sm-12">
            <h5 class="lead" >Adresa:</h5>
            Bd. Barbu Văcărescu nr. 164A, Bucureşti</br></br>
            <h5 class="lead" >Birou</h5>
            office@bit-soft.ro</br>
            + 40 021 242 6918</br></br>
            <h5 class="lead" >Vânzări</h5>
            sales@bit-soft.ro</br>
            +40 745 633 008</br>
        </div>

    </div>


</div>

    <div id="Oradea" class="tabcontent">
        <div class="row">
            <div class="col-md-8 col-sm-12">
             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2718.157048573939!2d21.891491115813384!3d47.056769533790046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4746477176ddd0df%3A0x791176a84759675c!2sBit%20Soft%20Oradea!5e0!3m2!1sro!2sro!4v1657697737734!5m2!1sro!2sro" style="border:0;width: 100%;position: relative;height:650px;margin-bottom" allowfullscreen="" loading="lazy"></iframe>
             
            </div>
            <div class="col-md-4 col-sm-12">
                <h5 class="lead" >Adresa:</h5>
                Str. Aviatorilor, nr.85, Oradea</br></br>
                <h5 class="lead" >Birou</h5>
                office@bit-soft.ro</br>
                + 40 021 242 6918</br></br>
                <h5 class="lead" >Vânzări</h5>
                sales@bit-soft.ro</br>
                +40 745 633 008</br>
            </div>

        </div>
    </div>

<div id="Budapesta" class="tabcontent">
        <div class="row">
            <div class="col-md-8 col-sm-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2697.215359181587!2d19.162106615823816!3d47.46623260565695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741c304e89f37a5%3A0x5f9e809ee65427f2!2sBit%20Soft!5e0!3m2!1sro!2sro!4v1657700627132!5m2!1sro!2sro" style="border:0;width: 100%;position: relative;height:650px;" allowfullscreen="" loading="lazy"></iframe>
            </div>
            <div class="col-md-4 col-sm-12">
                <h5 class="lead" >Adresa:</h5>
                Bányató utca 13, HU-1108, District 10, Budapesta</br></br>
                <h5 class="lead" >Birou</h5>
                support@bit-soft.hu</br>
                +36 1 677 7800</br></br>
                <h5 class="lead" >Vânzări</h5>
                sales@bit-soft.hu</br>
                +36 70 677 6002</br>
            </div>

        </div>

    </div>
</div>


</body>

<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tablinks = document.getElementsByClassName("tablinks");
    tabcontent = document.getElementsByClassName("tabcontent");
    if ($('.tabcontent:animated').length) {
        return false;
    }
    for (i = 0; i < tabcontent.length; i++) {
        if(tabcontent[i].style.display != "none"){
            $(tabcontent[i]).slideUp( "slow" , function() {
                // Animation complete.
                $("#"+document.getElementById(cityName).id).slideDown( "slow");
            });
            break;

        }
    }

    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


</html>