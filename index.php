<!DOCTYPE html>
<html lang="en">
<head>
<?php

  include("header.php");
  include_once("header_particles.php");
  include_once("footer_particles.php");
?>
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css"/>
<link rel="stylesheet" href="css/swiper.css">
</head>

<body>

<?php
include("navbar.php");
?>
<div>
  <div class="container1">

    <h6 class="display-4">Noutati!</h6>

    <hr class="my-4">

    <p class="lead"> Momentan, nu exista noutati de afisat.</p>

  </div>

  </br>
  <div class="container1">
    <h6 class="display-4">Cele mai vandute produse</h6>
    <hr class="my-4">
    <!-- Swiper -->
    <div class="swiper mySwiper">
      <div class="swiper-wrapper">
          <?php
            $sql = "SELECT  pr.*, SUM(`Cantitate`) AS Total FROM COMENZI_DETALII cd,PRODUCT pr
            WHERE pr.item_id = cd.item_id AND pr.display_product=1 GROUP BY Produs ORDER BY Total DESC LIMIT 10";
            $query = mysqli_query($dbconnect, $sql);

            while ($row = $query->fetch_assoc()) {

              $v[$row['item_id']]['item_id'] = $row['item_id'];
              $v[$row['item_id']]['item_name'] = $row['item_name'];
              $v[$row['item_id']]['item_price'] = $row['item_price'];
              $v[$row['item_id']]['item_image'] = $row['item_image'];
            }
              foreach ($v as $key=>$value) {
                
              echo '<div class="swiper-slide">';
                $image= $v[$key]['item_image'];
                echo "
              <div class='card' style='width:200px;margin:0 !important;' >
                <img src='$image' class='card-img-top' onerror='this.src =".'"'.$def_source.'"'."' alt='...'>
                <div class='card-body'>
                  <h5 class='card-title'>" . $v[$key]['item_name'] . "</h5>
                  <p class='card-text'>" . $v[$key]['item_price'] . "</p>
                </div>
                <div class='card-footer'>
                  <span class='card-footer-item' style='top: 35%;' id='produs_count'></span>
                </div>
              </div>";

              echo '</div>';
          
              }
        ?>
      </div>
      <div class="swiper-pagination"></div>
      <!-- <div class="swiper-button-prev"></div>
      <div class="swiper-button-next"></div> -->
    </div>

  </div>

  </br>
  <div class="container1">
    <h6 class="display-4">Categorii de Produse</h6>
    <hr class="my-4">
    <?php
    $sql1 = "SELECT * FROM CATEGORIES ";
    $query1 = mysqli_query($dbconnect, $sql1);

    echo '<button style="position:relative;display:inline;" onclick="location.href=\'produse.php\'" class="btn-gradient">All</button>';
    while ($row1 = $query1->fetch_assoc()) {
      echo '<button style="position:relative;display:inline;" onclick="location.href=\'produse.php?id_categorie='.$row1['id_categorie'].'\'" class="btn-gradient">'.$row1['den_categorie'].'</button>';
    }
    ?>
  </div>


</br>

</body>

    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>

    <!-- Initialize Swiper -->
    <script>
      var swiper = new Swiper(".mySwiper", {
        effect: "coverflow",
        coverflowEffect: {
          rotate: 20,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: false,
        },
        grabCursor: true,
        breakpoints: {
          // when window width is >= 320px
          320: {
            slidesPerView: 1,
            spaceBetween: 20
          },
          1200: {
            slidesPerView: 3,
            spaceBetween: 50
          }
        },

        pagination: {
          el: ".swiper-pagination",
          clickable: true,
          type: "bullets",
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
    </script>
</html>